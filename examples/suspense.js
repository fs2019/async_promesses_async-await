const readJSONwithSuspense = (fileName) =>
      readJSONprom(fileName).
      then(res => waitLogResolve(2, res)).
      then(null, err => waitLogReject(2, err))
