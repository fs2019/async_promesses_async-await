const food = require('./food')('/tmp/')

function testFoodFromFileProm(message, name){
  return Promise.resolve(message).
    then(console.log).
    then(() => food.fromFileProm(name)).
    then(res => console.log('res = ',res)).
    catch(err => console.log('err = ',err))
  
}

testFoodFromFileProm('1: file does not exist', 'pouet').
  then(() => testFoodFromFileProm('2 : file exists and contains correct json', 'ok')).
  then(() => testFoodFromFileProm('3 : file exists and contains bad json', 'bad'))

